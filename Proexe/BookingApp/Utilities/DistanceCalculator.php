<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator {

	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return mixed
	 */
	public function calculate( $from, $to, $unit = 'm' ) {
		$distance = 0.0;
		if( $from[0] == $to[0] && $from[1] == $to[1] ) {
			return $distance;
		}
		$radLatFrom = $this->getRadiansFromAngle( $from[0] );
		$radLatTo = $this->getRadiansFromAngle( $to[0] );
		$theta = $from[1] - $to[1];
		$radTheta = $this->getRadiansFromAngle( $theta );
		$distance = $this->calcualteDistanceUsingHaversinesLaw( $radLatFrom, $radLatTo, $radTheta);
		$distance = acos( $distance );
		$distance = $distance * 180/pi();
		$distance = $distance * 60 * 1.1515;
		if ( $unit == "km" ) $distance = $distance * 1.609344;
		if ( $unit == "m" ) $distance = $distance * 1609.344;
		return $distance;
	}
	//see https://en.wikipedia.org/wiki/Haversine_formula for more informations
	private function calcualteDistanceUsingHaversinesLaw( $a, $b, $c ) {
		$distance = sin( $a ) * sin( $b ) + cos( $a ) * cos( $b ) * cos( $c );
		if ($distance > 1.0) {
			$distance = 1.0;
		}
		return $distance;
	}
	private function getRadiansFromAngle( $angle ) {
		$radians = pi() * $angle/180;
		return $radians;
	}

	/**
	 * @param array $from
	 * @param array $offices
	 *
	 * @return array
	 */
	public function findClosestOffice( $from, $offices ) {
		$closestOffice = "";
		$smallestDist = INF;
		foreach ($offices  as $office ) {
			$distance = $this->calculate([ $from[0], $from[1] ], [ $office['lat'], $office['lng'] ], 'm' );
			if( $distance < $smallestDist ) {
				$closestOffice = $office['name'];
				$smallestDist = $distance;
			}
		}
		return $closestOffice;
	}
	/*
	Pure SQL for finding closest Office
		
	
	DECLARE @OfficeName string
	DECLARE @tmpOfficeName string
	DECLARE @OfficeDistance = 9398879740000.0
	DECLARE @tmpOfficeDistance float
	DECLARE @OfficeLat float
	DECLARE @OfficeLon float
	DECLARE @OfficeId int
	DECLARE MY_CURSOR CURSOR 
  		LOCAL STATIC READ_ONLY FORWARD_ONLY
	FOR 
	SELECT DISTINCT OfficeId 
	FROM Office

	OPEN MY_CURSOR
	FETCH NEXT FROM MY_CURSOR INTO @OfficeId
	WHILE @@FETCH_STATUS = 0
	BEGIN 
		SELECT @tmpOfficeName = Name,
		@OfficeLat = lat,
		@OfficeLon = on
		 FROM Office WHERE(Id = @OfficeId)

    	@tmpOfficeDistance = select ST_Distance_Sphere(
        point(POINT_LON, POINT_LAT),
		point(@OfficeLon, @OfficeLat))
		IF (@tmpOfficeDistance < @OfficeDistant)
		BEGIN
			@OfficeDistnace = @tmpOfficeDistance
			@tmpOfficeName = @OfficeName
		END
    	FETCH NEXT FROM MY_CURSOR INTO @OfficeId
	END
	CLOSE MY_CURSOR
	DEALLOCATE MY_CURSOR
	PRINT @OfficeName
	PRINT @OfficeDistance
	*/
}