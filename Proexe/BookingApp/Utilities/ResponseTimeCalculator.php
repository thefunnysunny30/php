<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use Carbon\Carbon;
use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface {

	public function calculate( $bookingDateTimeString, $responseDateTimeString, $officeHours ) {
		$bookingDateTime = new Carbon($bookingDateTimeString);
		$responseDateTime = new Carbon($responseDateTimeString);
		$daysDiff = $bookingDateTime->diffInDays($responseDateTime);
		$bookingDayIndex = $bookingDateTime->dayOfWeek;
		$responseDayIndex = $responseDateTime->dayOfWeek;

		if( $daysDiff == 0 && $bookingDayIndex == $responseDayIndex ) {
			return $bookingDateTime->diffInMinutes( $responseDateTime );
		}

		$daysIndexes = $this->getArrayOfDayIndexes( $bookingDayIndex, $responseDayIndex, $daysDiff );
		$indexesToEliminate = $this->getArrayOfDayIndexesWhenClosed( $officeHours );
		$daysIndexes = array_diff( $daysIndexes, $indexesToEliminate );
		$minutes = $this->calculateMinutesForDaysIndexArray( $daysIndexes, $bookingDayIndex, $responseDayIndex,
		 $officeHours, $bookingDateTime, $responseDateTime);
		return $minutes;
	}

	private function getArrayOfDayIndexes( $bookingDayIndex, $responseDayIndex, $daysDiff ) {
		$daysIndexes = [$bookingDayIndex];
		for( $dayIndex = $bookingDayIndex + 1, $i = 0; $i < $daysDiff; $i++, $dayIndex++ ){
			$dayIndex = $dayIndex % 7;
			array_push( $daysIndexes, $dayIndex );
		}
		if( end($daysIndexes) != $responseDayIndex ) {
			array_push( $daysIndexes, $responseDayIndex );
		}
		return $daysIndexes;
	}

	private function getArrayOfDayIndexesWhenClosed( $officeHours ) {
		$indexesToEliminate = [];
		for($i = 0; $i < count($officeHours); $i++) {
			if( $officeHours[ $i ]['isClosed'] ) {
				array_push( $indexesToEliminate, $i );
			}
		}
		return $indexesToEliminate;
	}

	private function calculateMinutesForDaysIndexArray( $daysIndexes, $bookingDayIndex, $responseDayIndex,
	 $officeHours, $bookingDayTime, $responseDayTime) {
		$minutes = 0;
		$counter = 0;
		foreach ( $daysIndexes  as $dayIndex ) {
			$begginingOfTheDayTime = Carbon::createFromFormat( "H:i", $officeHours[ $dayIndex ]['from'] );
			$endOfTheDayTime = Carbon::createFromFormat( "H:i", $officeHours[ $dayIndex ]['to'] );
			if($dayIndex == $bookingDayIndex && $counter == 0) {
				$endOfDay = $this->getGivenDateWithChangedHoursAndMinutes( $bookingDayTime, $officeHours[ $dayIndex ]['to'] );
				$minutes += $bookingDayTime->diffInMinutes( $endOfDay );
			}
			else if($dayIndex == $responseDayIndex && $counter == count($daysIndexes) - 1 ) {
				$startOfDay = $this->getGivenDateWithChangedHoursAndMinutes( $responseDayTime, $officeHours[ $dayIndex ]['from'] );
				$minutes += $startOfDay->diffInMinutes( $responseDayTime );
			}
			else {
				$minutes += $endOfTheDayTime->diffInMinutes( $begginingOfTheDayTime );
			}

			$counter++;
		}
		return $minutes;
	}
	private function getGivenDateWithChangedHoursAndMinutes( $dayTime, $hourMinutes ) {
		$date = $dayTime->format("Y-m-d H:i:s");
		$arr = explode(' ', $date);
		$hour = explode(':', $arr[1])[0];
		$mins = explode(':', $arr[1])[1];
		$newHour = explode(':', $hourMinutes)[0];
		$newMinutes = explode(':', $hourMinutes)[1];
		$hour = $newHour;
		$minutes = $newMinutes;
		$newDate = $arr[0] . " " . $hour . ":" . $minutes . ":" . explode(':', $arr[1])[2];
		return new Carbon($newDate);
	}
}