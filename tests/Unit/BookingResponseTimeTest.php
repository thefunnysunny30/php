<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Proexe\BookingApp\Utilities\ResponseTimeCalculator;

class BookingResponseTimeTest extends TestCase
{
    public function testResponseTimeCalculatorMinutesForSameDay()
    {
        $responseTimeCalculator = new ResponseTimeCalculator();
        $booking = [
            'created_at' => '2018-03-05 12:19:00',
            'updated_at' => '2018-03-05 12:29:00',
            'office' => [
                'office_hours' => [
                    [
                        "to"=> "18:00",
                        "from" => "11:00",
                        "isClosed" =>false
                    ],
                    [
                        "to"=> "18:00",
                        "from" => "11:00",
                        "isClosed" =>false
                    ],
                    [
                        "to"=> "18:00",
                        "from" => "11:00",
                        "isClosed" =>false
                    ],
                    [
                        "to"=> "18:00",
                        "from" => "11:00",
                        "isClosed" =>false
                    ],
                    [
                        "to"=> "18:00",
                        "from" => "11:00",
                        "isClosed" =>false
                    ],
                    [
                        "to"=> "18:00",
                        "from" => "11:00",
                        "isClosed" =>false
                    ],
                    [
                        "to"=> "18:00",
                        "from" => "11:00",
                        "isClosed" =>true
                    ],
                ]
            ],
        ];
        $expected = 10;
        $actual = $responseTimeCalculator->calculate($booking['created_at'],$booking['updated_at'] ,$booking['office']['office_hours']);
        $this->assertEquals( $expected, $actual );
    }

    public function testResponseTimeCalculatorMinutesForDifferentDay()
    {
        $responseTimeCalculator = new ResponseTimeCalculator();
        $booking = [
            'created_at' => '2018-03-05 12:00:00',
            'updated_at' => '2018-03-06 12:00:00',
            'office' => [
                'office_hours' => [
                    [
                        "to"=> "18:00",
                        "from" => "11:00",
                        "isClosed" =>false
                    ],
                    [
                        "to"=> "18:00",
                        "from" => "11:00",
                        "isClosed" =>false
                    ],
                    [
                        "to"=> "18:00",
                        "from" => "11:00",
                        "isClosed" =>false
                    ],
                    [
                        "to"=> "18:00",
                        "from" => "11:00",
                        "isClosed" =>false
                    ],
                    [
                        "to"=> "18:00",
                        "from" => "11:00",
                        "isClosed" =>false
                    ],
                    [
                        "to"=> "18:00",
                        "from" => "11:00",
                        "isClosed" =>false
                    ],
                    [
                        "to"=> "18:00",
                        "from" => "11:00",
                        "isClosed" =>false
                    ],
                ]
            ],
        ];
        $expected = 420;
        $actual = $responseTimeCalculator->calculate($booking['created_at'],$booking['updated_at'] ,$booking['office']['office_hours']);
        $this->assertEquals( $expected, $actual );
    }
}